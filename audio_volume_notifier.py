#!/usr/bin/python
"""
Run a daemon to respond to audio change with media keys on Openbox,
using keybinds as defined in ~/.config/openbox/rc.xml.

The script will trigger and update a notify-send notification with
a bar and an icon corresponding to the current audio volume by
keeping track of the replace id of the notification until the
notification expires.
"""
from re import compile as re_compile, findall
from subprocess import Popen, PIPE
import gobject
import daemon
import dbus
import dbus.glib
import syslog

def main():
    """
    Create a daemon context for the running DBus proxy.
    (It's not really a daemon yet, but we're getting there.)

    To shut down:
    > dbus-send --session --type=signal / com.test.StopListening
    """
    with daemon.DaemonContext():
        AudioVolumeNotifier()

class AudioVolumeNotifier(object):
    """
    This class acts as a proxy between incoming and outgoing signals on
    DBus, and keeps track of any live notifications it has spawned.
    """
    def __init__(self):
        """
        Initialize variables and set up listeners.
        """
        syslog.syslog(syslog.LOG_INFO, "Start receiving audio volume events.")
        self.replace_id = 0
        self.audio_regex = re_compile(r"(?<=\[)\w+(?=%?\])")
        self.audio_icon_sets = (
            ("audio-volume-low", "audio-volume-muted"),
            ("audio-volume-medium", "audio-volume-high")
        )
        session_bus = dbus.SessionBus()
        session_bus.add_signal_receiver(
            self.stop_listening,
            dbus_interface="com.test",
            signal_name="StopListening"
        )
        session_bus.add_signal_receiver(
            self.audio_volume_changed,
            dbus_interface="com.test",
            signal_name="MediaKeyAudioChange"
        )
        session_bus.add_signal_receiver(
            self.notification_closed,
            dbus_interface="org.freedesktop.Notifications",
            signal_name="NotificationClosed"
        )
        self.proxy = session_bus.get_object(
            "org.freedesktop.Notifications",
            "/org/freedesktop/Notifications"
        )
        self.loop = gobject.MainLoop()
        self.loop.run()

    def stop_listening(self):
        """
        Stop signal was sent. Stop listening for events.
        """
        syslog.syslog(syslog.LOG_INFO, "Stop receiving audio volume events.")
        self.loop.quit()

    def notification_closed(self, *args):
        """
        Notification was closed. Reset replace_id.
        """
        if not isinstance(args[0], dbus.UInt32):
            return
        replace_id = int(args[0])
        if replace_id == self.replace_id:
            syslog.syslog(syslog.LOG_DEBUG,
                          "Closed notification %d" % replace_id)
            self.replace_id = 0

    def audio_volume_changed(self):
        """
        Audio volume has changed. Update notification.
        """
        volume, muted = self.parse_amixer()
        icon = self.get_audio_icon(volume, muted)
        application_name = summary = "Volume Icon"

        value = self.proxy.Notify(
            application_name,
            dbus.UInt32(self.replace_id),
            icon, summary, "",
            dbus.Array([], signature="s"),
            dbus.Dictionary({
                'synchronous': 'volume',
                'value': volume
            }, signature="sv"), 5000
        )

        if not isinstance(value, dbus.UInt32):
            # I don't think this should ever happen
            syslog.syslog(syslog.LOG_ERR, "Something unexpected happened.")
            return

        self.replace_id = int(value)
        syslog.syslog(syslog.LOG_DEBUG,
                      "Updated notification %d" % self.replace_id)

    def get_audio_icon(self, volume, muted):
        """
        Return icon corresponding to current audio volume.
        """
        if muted:
            volume = 0
        medium_high = volume > 33
        muted_high = volume < 1 or volume > 84
        return self.audio_icon_sets[medium_high][muted_high]

    def parse_amixer(self):
        """
        Parse output from 'amixer -D pulse get Master'.
        """
        process = Popen(["amixer", "-D", "pulse", "get", "Master"], stdout=PIPE)

        line = process.stdout.readline()
        while line:
            values = findall(self.audio_regex, line)
            if values:
                percentage = int(values[0])
                muted = (False, True)[values[1] == "off"]
                return percentage, muted
            line = process.stdout.readline()
        return None, None

    def __del__(self):
        self.loop.quit()

if __name__ == "__main__":
    main()
