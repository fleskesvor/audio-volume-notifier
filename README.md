# audio-volume-notifier
Display notifications on audio volume change with media keys using Openbox.

**Introduction**

It turns out this functionality is already supported in Openbox by removing the keybinds in ~/.config/openbox/rc.xml mentioned below, and by changing the 'Hotkeys' section in ~/.config/volumeicon/volumeicon to:

```
[Hotkeys]
up_enabled=true
down_enabled=true
mute_enabled=true
```

I'll leave the repository up in case anyone might find it useful as an example on how to achieve something like this using Python. If I were to continue working on it, I would have changed the names of the scripts to make it clearer what they do.

**Dependencies**

The latest version has a few dependencies you might need to install before use:

* python-dbus
* python-daemon
* python-gobject

You can install these using your package manager of choice, or from the command line with apt-get:

`sudo apt-get install python-dbus python-daemon python-gobject`

**How to use**

* Download the scripts to your computer, either by cloning the repository or by downloading the sources ("Download ZIP" button) and unzipping them.

* Alter your ~/.config/openbox/rc.xml file to match these keybinds:

```xml
<keybind key="XF86AudioRaiseVolume">
  <action name="Execute">
    <command>sh ~/audio-volume-notifier/changeaudiovolume.sh 5%+</command>
  </action>
</keybind>
<keybind key="XF86AudioLowerVolume">
  <action name="Execute">
     <command>sh ~/audio-volume-notifier/changeaudiovolume.sh 5%-</command>
  </action>
</keybind>
<keybind key="XF86AudioMute">
  <action name="Execute">
    <command>sh ~/audio-volume-notifier/changeaudiovolume.sh toggle</command>
  </action>
</keybind>
```

Make sure the paths correspond to where the files are actually found on your computer.

* Restart Openbox:

`openbox --restart`

* Start the daemon that will update the audio volume notifications:

`python audio_volume_notifier.py`

* To shut down the daemon:

`dbus-send --session --type=signal / com.test.StopListening`

* If you encounter problems, output from syslog can be helpful:

`sudo tail -f /var/log/syslog`
